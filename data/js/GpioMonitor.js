// inital websocket
var gateway = `ws://${window.location.hostname}/ws`;

var websocket;
function initWebSocket() {
    console.log("Trying to open a WebSocket connection...");
    websocket = new WebSocket(gateway);
    websocket.onopen = onOpen;
    websocket.onclose = onClose;
    websocket.onmessage = onMessage; // <-- add this line
}
function onOpen(event) {
    console.log("Connection opened");
}
function onMessage(event) {
    console.log(event.data);
    let data = JSON.parse(event.data);

    if (data.RL1_state == 1) {
        document.getElementById("SW1").checked = true;
        document.getElementById("outputState1").innerHTML = "on";
    } else {
        document.getElementById("SW1").checked = false;
        document.getElementById("outputState1").innerHTML = "off";
    }
    if (data.RL2_state == 1) {
        document.getElementById("SW2").checked = true;
        document.getElementById("outputState2").innerHTML = "on";
    } else {
        document.getElementById("SW2").checked = false;
        document.getElementById("outputState2").innerHTML = "off";
    }
    if (data.RL3_state == 1) {
        document.getElementById("SW3").checked = true;
        document.getElementById("outputState3").innerHTML = "on";
    } else {
        document.getElementById("SW3").checked = false;
        document.getElementById("outputState3").innerHTML = "off";
    }
    if (data.RL4_state == 1) {
        document.getElementById("SW4").checked = true;
        document.getElementById("outputState4").innerHTML = "on";
    } else {
        document.getElementById("SW4").checked = false;
        document.getElementById("outputState4").innerHTML = "off";
    }
    if (data.RL5_state == 1) {
        document.getElementById("SW5").checked = true;
        document.getElementById("outputState5").innerHTML = "on";
    } else {
        document.getElementById("SW5").checked = false;
        document.getElementById("outputState5").innerHTML = "off";
    }
}
window.addEventListener("load", onLoad);
// window.addEventListener("getStateRelay", getStateRelay);
function onLoad(event) {
    initWebSocket();
    initButton();
    //getStateRelay();
}

function onClose(event) {
    console.log("Connection closed");
    setTimeout(initWebSocket, 2000);
}

// function getStateRelay(event) {
//     setInterval(() => websocket.send("getStateRelay"), 2000);
// }

function initButton() {
    document.getElementById("SW1").addEventListener("click", toggle1);
    document.getElementById("SW2").addEventListener("click", toggle2);
    document.getElementById("SW3").addEventListener("click", toggle3);
    document.getElementById("SW4").addEventListener("click", toggle4);
    document.getElementById("SW5").addEventListener("click", toggle5);
}
function toggle1() {
    websocket.send("toggleRL1");
}
function toggle2() {
    websocket.send("toggleRL2");
}
function toggle3() {
    websocket.send("toggleRL3");
}
function toggle4() {
    websocket.send("toggleRL4");
}
function toggle5() {
    websocket.send("toggleRL5");
}



$(document).ready(function () {


    // get  state input
    setInterval(function () {
        $.post("/getStateCoil", {}, function (response) {
            //console.log(response);
            //const obj = JSON.parse(response);
            $("#stateGPIO2").text(response.stateGPIO2 ? "high" : "low");
            $("#stateGPIO4").text(response.stateGPIO4 ? "high" : "low");
            $("#stateGPIO18").text(
                response.stateGPIO18 ? "high" : "low"
            );
            $("#stateGPIO19").text(
                response.stateGPIO19 ? "high" : "low"
            );
            $("#stateGPIO15").text(
                response.stateGPIO15 ? "high" : "low"
            );
        });
        updateDateTime();
    }, 2000);
});

