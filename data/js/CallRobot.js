if (!!window.EventSource) {
    var source = new EventSource('/events');

    source.addEventListener('open', function (e) {
        console.log("Events Connected");
    }, false);
    source.addEventListener('error', function (e) {
        if (e.target.readyState != EventSource.OPEN) {
            console.log("Events Disconnected");
        }
    }, false);

    source.addEventListener('getIdRobot', function (e) {
        const obj = JSON.parse(e.data);
        $("#notify-json").html(JSON.stringify(obj, null, 4));
        console.log("message", e.data);
    }, false);

    source.addEventListener('callRobotRun', function (e) {
        $("#notify-json").html(JSON.stringify(e.data, null, 4));
        console.log("message", e.data);
    }, false);

    source.addEventListener('httpGetURL', function (e) {
        $("#notify-json").html(JSON.stringify(e.data, null, 4));
        console.log("message", e.data);
    }, false);

}
$(document).ready(function () {
    $("form").each(function () {
        var form = $(this);
        form.submit(function (e) {

            var actionUrl = form.attr('action');

            console.log(form.serialize());
            $.ajax({
                type: "GET",
                url: actionUrl,
                data: form.serialize(), // serializes the form's elements.
                success: function (data) {
                    console.log(data); // show response from the php script.
                    $("#notify-json").html(JSON.stringify(data, null, 4));
                }
            });
            event.preventDefault();

        });
    });
    $(".button1").click(function () {
        $('.button1').prop('disabled', true);
        $.post("/getIdRobot", {}, function (response) {


            setTimeout(function () {
                $('.button1').prop('disabled', false);
            }, 2000);
            //const obj = JSON.parse(response);
        });
    });

    $(".button2").click(function () {
        $('.button2').prop('disabled', true);
        $.post("/callRobotRun", {}, function (response) {
            console.log(response);
            setTimeout(function () {
                $('.button2').prop('disabled', false);
            }, 2000)
        });
    });
    // this is the id of the form
});