#ifndef __CALLROBOT__
#define __CALLROBOT__

#include <HTTPClient.h>
#include "../lib/ArduinoJson/ArduinoJson.h"

enum StationStatus
{
    waitting,
    gohome,
    freeMission,
    unknown,
};

class CallRobot
{
private:
    // HTTPClient HttpClient;
    String _Ip = "";
    String _Model = "";
    String _MapName = "";
    int _Port = 0;
    String RobotId = "";
    String StationName = "station-1";
    String LineName = "station-7";
    String TaskStationName = "NavigationTo";
    String TaskLineName = "NavigationLine";

    String StatusRobot = "StatusAGV";
    String ProcessingRobot = "ProcessingAGV";
    String RobotModel = "agv-500";
    String MapName = "demo-f1";

    // String StatusRobot ="Status";
    // String ProcessingRobot ="Processing";
    // String RobotModel = "amr-250";
    // String MapName = "ledlighting2908";

public:
    CallRobot(String ip, int port)
    {
        _Ip = ip;
        _Port = port;
        _Model = RobotModel;
        _MapName = MapName;
    };
    CallRobot(String ip, int port, String RobotModel, String MapName)
    {
        _Ip = ip;
        _Port = port;
        _Model = RobotModel;
        _MapName = MapName;
    };
    ~CallRobot();

    StationStatus StationStatusRuntime = StationStatus::freeMission;
    String HttpGet(String url);
    String HttpPost(String url, String data);
    String HttpDelete(String url);
    String getIdRobot(String statusRobot, String taskName, String processRobot);
    String getState(String stateName, String robotId);
    int getTask(String taskName, String robotId);
    bool runTask(String taskName, String pointName, String robotId);
    bool CancelTask(String taskName, String robotId);
    bool CallingRobot();
    bool GoHomeRobot();
    bool CancelAction();

    void setStationName(const char *input);
    String getStationName(void);

    void setLineName(const char *input);
    String getLineName(void);
};

#endif