#ifndef __GPIO_H__
#define __GPIO_H__
#include "Arduino.h"
#define RL_1 27
#define RL_2 26
#define RL_3 14
#define RL_4 13
#define RL_5 23

#define AO_1 32
#define AO_2 33
#define AO_3 25

#define BUZZ 5
#define STAT 12

#define BTN_SET 0
#define SW_1 34
#define SW_2 35

#define RL_1_ON digitalWrite(RL_1, HIGH)
#define RL_2_ON digitalWrite(RL_2, HIGH)
#define RL_3_ON digitalWrite(RL_3, HIGH)
#define RL_4_ON digitalWrite(RL_4, HIGH)
#define RL_5_ON digitalWrite(RL_5, HIGH)
#define RL_1_OFF digitalWrite(RL_1, LOW)
#define RL_2_OFF digitalWrite(RL_2, LOW)
#define RL_3_OFF digitalWrite(RL_3, LOW)
#define RL_4_OFF digitalWrite(RL_4, LOW)
#define RL_5_OFF digitalWrite(RL_5, LOW)

#define BUZZ_ON digitalWrite(BUZZ, HIGH)
#define STAT_ON digitalWrite(STAT, HIGH)

#define BUZZ_OFF digitalWrite(BUZZ, LOW)
#define STAT_OFF digitalWrite(STAT, LOW)

#define SerialComputer Serial
#define SerialModbus Serial2

const int freq = 5000;
const int Channel_1 = 1;
const int Channel_2 = 2;
const int Channel_3 = 3;
const int resolution = 8;
// Modbus Registers Offsets for Coil
const int COIL_M0 = 2049;
const int COIL_M10 = 2059;
const int COIL_M12 = 2061;

// Set pin input pullup
const int GPIO18 = 18; // GPIO18
const int GPIO19 = 19; // GPIO19
const int GPIO4 = 4;   // GPIO4

// Set pin input (because it connect pulldown on board)
const int GPIO2 = 2;
const int GPIO15 = 15;
// Stores LED state

void initGpio();
#endif /* !Gpio_H */