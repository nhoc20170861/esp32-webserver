#include "Gpio.h"
void initGpio()
{
    pinMode(STAT, OUTPUT);
    pinMode(BUZZ, OUTPUT);
    pinMode(RL_1, OUTPUT);
    pinMode(RL_2, OUTPUT);
    pinMode(RL_3, OUTPUT);
    pinMode(RL_4, OUTPUT);
    pinMode(RL_5, OUTPUT);

    pinMode(BTN_SET, INPUT_PULLUP);
    pinMode(SW_1, INPUT_PULLUP);
    pinMode(SW_2, INPUT_PULLUP);

    pinMode(GPIO18, INPUT_PULLUP);
    pinMode(GPIO19, INPUT_PULLUP);
    pinMode(GPIO4, INPUT_PULLUP);

    // 2 chân này kéo xuống GND qua trở ngoài
    pinMode(GPIO2, INPUT);
    pinMode(GPIO15, INPUT);

    ledcSetup(Channel_1, freq, resolution);
    ledcSetup(Channel_2, freq, resolution);
    ledcSetup(Channel_3, freq, resolution);

    ledcAttachPin(AO_1, Channel_1);
    ledcAttachPin(AO_2, Channel_2);
    ledcAttachPin(AO_3, Channel_3);
}