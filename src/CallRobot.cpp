#include "CallRobot.h"

String CallRobot::getStationName(void)
{
  return StationName;
}

void CallRobot::setStationName(const char *input)
{
  StationName = input;
  Serial.println("StationName: " + StationName);
}

String CallRobot::getLineName(void)
{
  return LineName;
}

void CallRobot::setLineName(const char *input)
{
  LineName = input;
  Serial.println("LineName: " + LineName);
}

String CallRobot::HttpGet(String url)
{

  if ((WiFi.status() == WL_CONNECTED))
  {
    HTTPClient http;
    http.begin(url);
    int httpCode = http.GET();
    String Json = "";
    if (httpCode > 0)
    {
      Serial.print("HTTP Response code: ");
      Serial.println(httpCode);
      Json = http.getString();
    }
    else
    {
      Serial.print("Error code: ");
      Serial.println(httpCode);
      Json = "[GET] Data Error: " + String(httpCode);
    }
    http.end();
    return Json;
  }

  return "WiFi lost";
}

String CallRobot::HttpPost(String url, String data)
{
  if ((WiFi.status() == WL_CONNECTED))
  {
    HTTPClient http;
    http.begin(url);
    int httpCode = http.POST(data);
    String Json = "";
    if (httpCode > 0)
    {
      Serial.print("HTTP Response code: ");
      Serial.println(httpCode);
      Json = http.getString();
    }
    else
    {
      Serial.print("Error code: ");
      Serial.println(httpCode);
      Json = "[POST] Data Error: " + String(httpCode);
    }
    http.end();
    return Json;
  }

  return "WiFi lost";
}

String CallRobot::HttpDelete(String url)
{
  if ((WiFi.status() == WL_CONNECTED))
  {
    HTTPClient http;
    http.begin(url);
    int httpCode = http.sendRequest("Delete");
    String Json = "";
    if (httpCode > 0)
    {
      Json = http.getString();
    }
    http.end();
    return Json;
  }
  return "";
}

String CallRobot::getIdRobot(String statusRobot, String taskName, String processRobot)
{
  String url = "http://" + _Ip + ":" + _Port + "/api/Remote/Robots?model=" + _Model + "&map=" + _MapName;
  String DataResult = HttpGet(url);
  if (DataResult == "")
    return "";
  DynamicJsonDocument doc(1024);
  deserializeJson(doc, DataResult);
  for (int i = 0; i < 2; i++)
  {
    const char *id = doc[i]["id"];
    if (id == "")
      break;
    String Status = getState(statusRobot, id);
    if (Status != "Stopping")
      continue;
    int ProcessTask = getTask(taskName, id);
    if (ProcessTask == 2)
      continue;
    String ProcessRobot = getState(processRobot, id);
    if (ProcessRobot == "Running")
      continue;
    return id;
  }
  return "";
}

String CallRobot::getState(String stateName, String robotId)
{
  String url = "http://" + _Ip + ":" + _Port + "/api/Remote/Robot/" + robotId + "/State/" + stateName;
  String DataResult = HttpGet(url);
  if (DataResult == "")
    return "";
  DynamicJsonDocument doc(1024);
  deserializeJson(doc, DataResult);
  const char *result = doc["result"];
  bool isError = doc["isError"];
  if (isError)
    return "";
  return result;
}

int CallRobot::getTask(String taskName, String robotId)
{
  String url = "http://" + _Ip + ":" + _Port + "/api/Remote/Robot/" + robotId + "/Task/" + taskName;
  String DataResult = HttpGet(url);
  if (DataResult == "")
    return 0;
  DynamicJsonDocument doc(1024);
  deserializeJson(doc, DataResult);
  int result = doc["result"]["status"];
  bool isError = doc["isError"];
  if (isError)
    return 0;
  return result;
}

bool CallRobot::runTask(String taskName, String pointName, String robotId)
{
  String url = "http://" + _Ip + ":" + _Port + "/api/Remote/Robot/" + robotId + "/Task/" + taskName + "?args=" + pointName;
  pointName = "args=" + pointName;
  String DataResult = HttpPost(url, pointName);
  if (DataResult == "")
    return NULL;
  DynamicJsonDocument doc(1024);
  deserializeJson(doc, DataResult);
  bool isError = doc["isError"];
  return ~isError;
}

bool CallRobot::CancelTask(String taskName, String robotId)
{
  String url = "http://" + _Ip + ":" + _Port + "/api/Remote/Robot/" + robotId + "/Task/" + taskName;
  Serial.println(url);
  String DataResult = HttpDelete(url);
  Serial.println(DataResult);
  if (DataResult == "")
    return NULL;
  DynamicJsonDocument doc(1024);
  deserializeJson(doc, DataResult);
  bool isError = doc["isError"];
  return isError;
}

bool CallRobot::CallingRobot()
{
  RobotId = getIdRobot(StatusRobot, TaskStationName, ProcessingRobot);
  if (RobotId == "")
    return false;
  bool result = runTask(TaskStationName, StationName, RobotId);
  if (!result)
    return false;
  StationStatusRuntime = StationStatus::waitting;
  return true;
}

bool CallRobot::GoHomeRobot()
{
  int navigationTaskStatus = getTask(TaskStationName, RobotId);
  if (navigationTaskStatus == 2)
    return false;
  String navigataionTaskComplete = getState(ProcessingRobot, RobotId);
  if (navigataionTaskComplete != "Done")
    return false;
  bool result = runTask(TaskLineName, LineName, RobotId);
  if (!result)
    return false;
  StationStatusRuntime = StationStatus::gohome;
  RobotId = "";
  return true;
}

bool CallRobot::CancelAction()
{
  if (StationStatusRuntime == StationStatus::waitting)
  {
    bool result = CancelTask(TaskStationName, RobotId);
  }
  if (StationStatusRuntime == StationStatus::gohome)
  {
    bool result = CancelTask(TaskLineName, RobotId);
  }
  StationStatusRuntime = StationStatus::freeMission;
  RobotId = "";
  return true;
}