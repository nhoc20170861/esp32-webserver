#include <ModbusIP_ESP8266.h>
#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>
#include <AsyncElegantOTA.h>
#include "EEPROM.h"
#include "SPIFFS.h"
#include "../include/Gpio.h"
#include "../include/EspSPIFFS.h"
#include "../include/CallRobot.h"

// define the number of bytes you want to access
#define EEPROM_SIZE 4

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// initialize websocket
AsyncWebSocket ws("/ws");

// Create an Event Source on /events
AsyncEventSource events("/events");

//******************************************************************************//
CallRobot *CallRobotObject = new CallRobot("172.20.2.50", 8080);

//------------------------------------------------------------
EspSPIFFS *espSPIFFS = new EspSPIFFS();

// IP_ModbusTcp Server
ModbusIP mb;

// Search for parameter in HTTP POST request
const char *PARAM_INPUT_SSID = "ssid";
const char *PARAM_INPUT_PASS = "pass";
const char *PARAM_INPUT_IP = "ip";
const char *PARAM_INPUT_GATEWAY = "gateway";

// http authentication
const char *http_username = "robotnet";
const char *http_password = "Robotnet@2022";

// Variables to save values from HTML form
String ssid;
String pass;
String ip;
String gateway;

IPAddress localIP;
// IPAddress localIP(192, 168, 1, 200); // hardcoded

// Set your Gateway IP address
IPAddress localGateway;

// IPAddress localGateway(192, 168, 1, 1); //hardcoded
IPAddress subnet(255, 255, 254, 0);

IPAddress primaryDNS(8, 8, 8, 8);   // optional
IPAddress secondaryDNS(8, 8, 4, 4); // optional

// Set number of relays
#define NUM_RELAYS 5

// array to store relay sate
bool relayOutput[NUM_RELAYS] = {0, 0, 0, 0, 0};

// Assign each GPIO to a relay
int relayGPIOs[NUM_RELAYS] = {RL_1, RL_2, RL_3, RL_4, RL_5};

//-------------------------------------------
void handleWebSocketMessage(void *arg, uint8_t *data, size_t len);

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
             void *arg, uint8_t *data, size_t len);

//------------------------
void initWebSocket()
{
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

//------------------------
void initEventSource()
{
  // Handle Web Server Events
  events.onConnect([](AsyncEventSourceClient *client)
                   {
    if(client->lastId()){
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
    }
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello!", NULL, millis(), 10000); });

  server.addHandler(&events);
}

// Variable to store wifimanager status
bool wifimanager_status = false;

// Initialize WiFi
bool initWiFi();
void wifimanager_start();

// Get relay State
String relayState(int numRelay)
{

  if (digitalRead(relayGPIOs[numRelay - 1]))
  {
    return "checked";
  }
  else
  {
    return "";
  }
}
// Get coil input state
String coilState(int numCoil)
{

  if (digitalRead(numCoil))
  {
    return "high";
  }
  else
  {
    return "low";
  }
}

// Replaces placeholder
String processorCallRobot(const String &var)
{
  if (var == "RSSI")
  {
    return String(WiFi.RSSI());
  }
  else if (var == "IP")
  {
    return ip;
  }
  else if (var == "StationName")
  {
    return CallRobotObject->getStationName();
  }
  else if (var == "LineName")
  {
    return CallRobotObject->getLineName();
  }
  else if (var == "CountReconnectWiFi")
  {
    int count = (int)EEPROM.read(0);
    Serial.println("wifi reconnection times: " + String(count));
    return String(count);
  }

  return String();
}

// Replaces placeholder
String processorGpioMonitor(const String &var)
{
  if (var == "BUTTONPLACEHOLDER")
  {
    String buttons = "";
    for (int i = 1; i <= NUM_RELAYS; i++)
    {
      String relayStateValue = relayState(i);
      String relayStatus = (relayStateValue == "checked") ? "on" : "off";
      buttons += "<h4>Relay #" + String(i) + " - GPIO " + relayGPIOs[i - 1] + " - State: " +
                 "<span id=\"outputState" + String(i) + "\"" + ">" + relayStatus + "</span>" +
                 "</h4><label class=\"switch\"><input type=\"checkbox\" id=\"SW" + String(i) + "\" " + relayStateValue + "><span class=\"slider\"></span></label>";
    }
    return buttons;
  }
  else if (var == "RSSI")
  {
    return String(WiFi.RSSI());
  }
  else if (var == "IP")
  {
    return ip;
  }
  else if (var == "STATE_GPIO2")
  {
    return coilState(GPIO2);
  }
  else if (var == "STATE_GPIO4")
  {
    return coilState(GPIO4);
  }
  else if (var == "STATE_GPIO15")
  {
    return coilState(GPIO15);
  }
  else if (var == "STATE_GPIO18")
  {
    return coilState(GPIO18);
  }
  else if (var == "STATE_GPIO19")
  {
    return coilState(GPIO19);
  }
  return String();
}

void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Not found");
}

// Pram input for requset, example : ?input1=<inputMessage>
const char *PARAM_INPUT_URL = "inputURL";
const char *PARAM_INPUT_StationName = "StationName";
const char *PARAM_INPUT_LineName = "LineName";

// declare object for taskhandle
static TaskHandle_t Task1 = NULL;
static TaskHandle_t Task2 = NULL;

// Globals
// static SemaphoreHandle_t mutex;

int taskMission = 0;
String httpURL = "";

void Task1code(void *pvParameters)
{
  unsigned long timeRequestButtonCurent = millis();
  Serial.println("task1 start on:" + String(xPortGetCoreID()));
  bool result = false;
  while (1)
  {

    switch (taskMission)
    {
    case 1: // Call Robot
    {
      String message = "";
      // thuc hien 1 nhiem vu gi do
      switch (CallRobotObject->StationStatusRuntime)
      {

      case StationStatus::freeMission:
      {
        result = CallRobotObject->CallingRobot();
        vTaskDelay(100 / portTICK_PERIOD_MS);
        // Send Events to the Web Server with the Sensor Readings
        message = "RobotfreeMission: " + String(result);
        events.send(message.c_str(), "callRobotRun", millis());
        break;
      }
      case StationStatus::gohome:
      {
        result = CallRobotObject->CallingRobot();
        vTaskDelay(100 / portTICK_PERIOD_MS);
        message = "Robot gohome: " + String(result);
        events.send(message.c_str(), "callRobotRun", millis());
        break;
      }
      case StationStatus::waitting:
      {
        result = CallRobotObject->GoHomeRobot();
        vTaskDelay(100 / portTICK_PERIOD_MS);

        // Send Events to the Web Server with the Sensor Readings
        message = "Robot waiting: " + String(result);
        events.send(message.c_str(), "callRobotRun", millis());
        break;
      }
      default:
      {
        Serial.println("defaults");
      }
      }
      taskMission = 0;
      break;
    }

    case 2: // GET id robot
    {
      String response = CallRobotObject->HttpGet("http://172.20.2.50:8080/api/Remote/Robots?model=agv-500&map=demo-f1");
      // Serial.println(response);
      events.send(response.c_str(), "getIdRobot", millis());
      vTaskDelay(50 / portTICK_PERIOD_MS);
      taskMission = 0;
      break;
    }
    case 3:
    {
      String response = CallRobotObject->HttpGet(httpURL.c_str());
      Serial.println(response);
      events.send(response.c_str(), "httpGetURL", millis());
      vTaskDelay(100 / portTICK_PERIOD_MS);
      taskMission = 0;
    }
    }

    // Serial.print("HighWaterMark:");
    // Serial.println(uxTaskGetStackHighWaterMark(NULL));
    vTaskDelay(20 / portTICK_PERIOD_MS);
  }
  vTaskDelete(NULL);
}

void TaskReconnectWiFi(void *pvParameters)
{
  int count_retry_connect = 0;
  unsigned long previousMillis_reconnectWifi = 0;
  bool task1_suspend = false;
  while (1)
  {
    // put your main code here, to run repeatedly:
    unsigned long currentMillis = millis();

    // if WiFi is down, tryreconnecting
    if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis_reconnectWifi >= 10000) && (wifimanager_status == false))
    {

      Serial.print(millis());
      Serial.println(" Reconnecting to WiFi...");

      task1_suspend = true;
      vTaskSuspend(Task1); // suspend Task1code

      WiFi.disconnect();
      WiFi.reconnect();
      BUZZ_ON;
      vTaskDelay(100 / portTICK_PERIOD_MS);
      BUZZ_OFF;

      count_retry_connect++;

      EEPROM.write(0, count_retry_connect);
      EEPROM.commit();
      Serial.println("store count_retry_connect variable to flash memory");
      previousMillis_reconnectWifi = currentMillis;
      if (count_retry_connect > 20)
      {

        Serial.println("Unable to reconnect to WiFi -> Start AP again");
        wifimanager_status = true;

        wifimanager_start();
      }
    }
    if ((WiFi.status() == WL_CONNECTED) && (wifimanager_status == false))
    {

      if (task1_suspend == true)
      {
        Serial.println("Reconnect to WiFi Success -> Task1code resum");
        vTaskResume(Task1); // resume Task1code
        task1_suspend = false;
      }
    }
    ws.cleanupClients();
    // Serial.print("HighWaterMark2:");
    // Serial.println(uxTaskGetStackHighWaterMark(NULL));
    // Serial.print("Free heap (bytes): ");
    // Serial.println(xPortGetFreeHeapSize());
    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
  vTaskDelete(NULL);
}

void Wifi_connected(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("Successfully connected to Access Point:  " + String(ssid));

  BUZZ_ON;
  delay(200);
  BUZZ_OFF;
  STAT_OFF;
}

void Get_IPAddress_RSSI(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("WIFI is connected!");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  long rssi = WiFi.RSSI();
  Serial.print("RSSI:");
  Serial.println(rssi);
}

void printEspReasonReset(void);

void setup()
{
  Serial.begin(115200);
  SerialModbus.begin(9600);
  while (!Serial)
    ;
  initGpio();
  printEspReasonReset();
  BUZZ_OFF;
  STAT_ON;

  // initialize EEPROM with predefined size
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.write(0, 0);
  EEPROM.commit();

  RL_1_OFF;
  RL_2_OFF;
  RL_3_OFF;
  RL_4_OFF;
  RL_5_OFF;

  ledcWrite(Channel_1, 0);
  ledcWrite(Channel_2, 0);
  ledcWrite(Channel_3, 0);

  // delete old config
  WiFi.disconnect(true);
  WiFi.onEvent(Wifi_connected, ARDUINO_EVENT_WIFI_STA_CONNECTED);
  WiFi.onEvent(Get_IPAddress_RSSI, ARDUINO_EVENT_WIFI_STA_GOT_IP);

  vTaskDelay(2000 / portTICK_PERIOD_MS);

  espSPIFFS->initSPIFFS();

  // Read field ssid, pass, ip , gateway
  ssid = espSPIFFS->readSSID(SPIFFS);
  pass = espSPIFFS->readPASS(SPIFFS);
  ip = espSPIFFS->readIP(SPIFFS);
  gateway = espSPIFFS->readGATEWAY(SPIFFS);

  Serial.println(ssid);
  Serial.println(pass);
  Serial.println(ip);
  Serial.println(gateway);

  // Read pin status of "Switch 2 bit"
  // if 2 pin is true, start wifimanger
  // else checking wifi connection through function iniWiFi()
  wifimanager_status = digitalRead(SW_1) & digitalRead(SW_2);
  Serial.println("wifimanager_status:" + String(wifimanager_status));

  if (wifimanager_status == true)
  {
    wifimanager_start();
  }
  else if (initWiFi())
  {
    // initEspmDNS();
    STAT_ON;
    server.serveStatic("/", SPIFFS, "/");
    // Route for root / web page
    // server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
    //           { request->redirect("/GpioMonitor"); });
    server.on("/GpioMonitor", HTTP_GET, [](AsyncWebServerRequest *request)
              { 
                if(!request->authenticate(http_username, http_password))
                    return request->requestAuthentication();
                request->send(SPIFFS, "/views/GpioMonitor.html", "text/html", false, processorGpioMonitor); });

    // Route for CallRobot web page
    server.on("/CallRobot", HTTP_GET, [](AsyncWebServerRequest *request)
              { 
                if(!request->authenticate(http_username, http_password))
                    return request->requestAuthentication();
                request->send(SPIFFS, "/views/CallRobot.html", "text/html", false, processorCallRobot); });

    // Route to receive state of relays after update done
    server.on("/getStateRelay", HTTP_POST, [](AsyncWebServerRequest *request)
              {
                String json = "{";
                json += "\"RL1_state\":" +String(digitalRead(RL_1));
                json += ",\"RL2_state\":"+String(digitalRead(RL_2));
                json += ",\"RL3_state\":"+String(digitalRead(RL_3));
                json += ",\"RL4_state\":"+String(digitalRead(RL_4));
                json += ",\"RL5_state\":"+String(digitalRead(RL_5));
                json +="}";
                //Serial.println(json);
                request->send(200, "application/json", json);
                json = String(); });
    // Route to receive state of coils input
    server.on("/getStateCoil", HTTP_POST, [](AsyncWebServerRequest *request)
              { 
                String json = "{";
                json += "\"stateGPIO2\":" +String(digitalRead(GPIO2));
                json += ",\"stateGPIO4\":"+String(digitalRead(GPIO4));
                json += ",\"stateGPIO15\":"+String(digitalRead(GPIO15));
                json += ",\"stateGPIO18\":"+String(digitalRead(GPIO18));
                json += ",\"stateGPIO19\":"+String(digitalRead(GPIO19));
                json +="}";
                //Serial.println(json);
                request->send(200, "application/json", json);
                json = String(); });

    server.on("/getIdRobot", HTTP_POST, [](AsyncWebServerRequest *request)
              { 
              taskMission = 2;
               
                Serial.println("getidRobot");
                request->send(200, "text/html", "Set getIdRobot success!"); });

    server.on("/callRobotRun", HTTP_POST, [](AsyncWebServerRequest *request)
              {
         taskMission = 1;

          request->send(200, "text/html", "Set callRobot true success"); });

    // Send a GET request to <ESP_IP>/httpGetURL?inputURL=<inputMessage>
    server.on("/httpGetURL", HTTP_GET, [](AsyncWebServerRequest *request)
              {
      
      String inputMessage = request->getParam(PARAM_INPUT_URL)->value();

      taskMission = 3;
      httpURL = inputMessage;

      String response = "Waiting for Esp Call URL";
      request->send(200, "text/html", response.c_str()); });

    // Send a GET request to <ESP_IP>/httpSetPram?StationName=<inputMessage1>&LineName=<inputMessage2>
    server.on("/httpSetPram", HTTP_GET, [](AsyncWebServerRequest *request)
              {
                String inputMessage1 = "";
                String inputMessage2 = "";
                if (request->hasParam(PARAM_INPUT_StationName) && request->hasParam(PARAM_INPUT_LineName))
                {
                  inputMessage1 = request->getParam(PARAM_INPUT_StationName)->value();
                  inputMessage2 = request->getParam(PARAM_INPUT_LineName)->value();

                  CallRobotObject->setStationName(inputMessage1.c_str());
                  CallRobotObject->setLineName(inputMessage2.c_str());
                  request->send(200, "text/html", "Set Pram success");
                }
                else
                {
                  
                  request->send(200, "text/html", "Set Pram error");
                } });

    server.onNotFound(notFound);
    // Start ElegantOTA

    AsyncElegantOTA.begin(&server, http_username, http_password);

    initWebSocket();
    initEventSource();

    server.begin();
  }
  else
  {
    wifimanager_status = true;
    wifimanager_start();
  }

  // Start Modbus TCP server
  if (WiFi.status() == WL_CONNECTED)
  {
    mb.server();
  }

  xTaskCreatePinnedToCore(
      TaskReconnectWiFi,   /* Task function. */
      "TaskReconnectWiFi", /* name of task. */
      1024 * 4,            /* Stack size of task (byte in ESP32) */
      NULL,                /* parameter of the task */
      2,                   /* priority of the task */
      &Task2,              /* Task handle */
      0);                  /* Run on one core*/
  xTaskCreatePinnedToCore(
      Task1code, /* Task function. */
      "Task1",   /* name of task. */
      2048 * 2,  /* Stack size of task (byte in ESP32) */
      NULL,      /* parameter of the task */
      3,         /* priority of the task */
      &Task1,    /* Task handle */
      1);        /* Run on one core*/

  Serial.println("Done!");

  // if (mb.addCoil(COIL_M0))
  // {
  //   Serial.println("Add Coil_M0 with address: " + String(COIL_M0) + " with value: " + String(mb.Coil(COIL_M0)));
  // }
  // if (mb.addCoil(COIL_M10))
  // {
  //   Serial.println("Add Coil_M0 with address: " + String(COIL_M10) + " with value: " + String(mb.Coil(COIL_M10)));
  // }
  // if (mb.addCoil(COIL_M12))
  // {
  //   Serial.println("Add Coil_M0 with address: " + String(COIL_M12) + " with value: " + String(mb.Coil(COIL_M12)));
  // }
}

void loop()
{

  // Call once inside loop() - all magic here
  // mb.task();

  // if (mb.Coil(COIL_M0) == true)
  // {
  //   RL_2_ON;
  // }
  // else
  //   RL_2_OFF;
  // delay(100);
}

void SendRelayStates()
{
  DynamicJsonDocument doc(1024);
  doc["RL1_state"] = digitalRead(RL_1);
  doc["RL2_state"] = digitalRead(RL_2);
  doc["RL3_state"] = digitalRead(RL_3);
  doc["RL4_state"] = digitalRead(RL_4);
  doc["RL5_state"] = digitalRead(RL_5);
  char data[100];
  size_t len = serializeJson(doc, data);
  ws.textAll(data, len);
}

void notifyClients(const char *str, int i)
{
  ws.textAll(str + String(relayOutput[i - 1]));
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
             void *arg, uint8_t *data, size_t len)
{
  switch (type)
  {
  case WS_EVT_CONNECT:
    Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
    break;
  case WS_EVT_DISCONNECT:
    Serial.printf("WebSocket client #%u disconnected\n", client->id());
    break;
  case WS_EVT_DATA:
    handleWebSocketMessage(arg, data, len);
    break;
  case WS_EVT_PONG:
  case WS_EVT_ERROR:
    break;
  }
}
void handleWebSocketMessage(void *arg, uint8_t *data, size_t len)
{
  AwsFrameInfo *info = (AwsFrameInfo *)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT)
  {
    data[len] = 0;

    char str[20];
    sprintf(str, "%s", data);
    Serial.println(str);
    if (strcmp((char *)data, "toggleRL1") == 0)
    {
      relayOutput[0] = !relayOutput[0];
      digitalWrite(relayGPIOs[0], relayOutput[0]);
    }
    else if (strcmp((char *)data, "toggleRL2") == 0)
    {
      relayOutput[1] = !relayOutput[1];
      digitalWrite(relayGPIOs[1], relayOutput[1]);
    }
    else if (strcmp((char *)data, "toggleRL3") == 0)
    {
      relayOutput[2] = !relayOutput[2];
      digitalWrite(relayGPIOs[2], relayOutput[2]);
    }
    else if (strcmp((char *)data, "toggleRL4") == 0)
    {
      relayOutput[3] = !relayOutput[3];
      digitalWrite(relayGPIOs[3], relayOutput[3]);
    }
    else if (strcmp((char *)data, "toggleRL5") == 0)
    {

      relayOutput[4] = !relayOutput[4];
      digitalWrite(relayGPIOs[4], relayOutput[4]);
    }
    SendRelayStates();
  }
}

// Timer variables for wating connect wifi
unsigned long previousMillis_cnWifi = 0;

bool initWiFi()
{

  const long interval = 10000; // interval to wait for Wi-Fi connection (milliseconds)

  if (ssid == "" || ip == "")
  {
    Serial.println("Undefined SSID or IP address.");
    return false;
  }

  WiFi.mode(WIFI_STA);
  // // -------------- Config ESP Wifi_sta with ip static--------
  localIP.fromString(ip.c_str());
  localGateway.fromString(gateway.c_str());

  if (!WiFi.config(localIP, localGateway, subnet, primaryDNS, secondaryDNS))
  {
    Serial.println("STA Failed to configure");
    return false;
  }

  Serial.println(WiFi.getHostname());

  WiFi.begin(ssid.c_str(), pass.c_str());
  Serial.println("Connecting to WiFi...");

  unsigned long currentMillis = millis();
  previousMillis_cnWifi = currentMillis;

  while (WiFi.status() != WL_CONNECTED)
  {
    currentMillis = millis();
    if (currentMillis - previousMillis_cnWifi >= interval)
    {
      Serial.println("Failed to connect.");
      return false;
    }
  }
  return true;
}

unsigned long previousMillis = 0;
void wifimanager_start()
{

  // vTaskSuspend(Task1);
  // vTaskSuspend(Task2);

  // Connect to Wi-Fi network with SSID and password
  Serial.println("Setting AP (Access Point)");
  // NULL sets an open Access Point
  WiFi.softAP("ESP-WIFI-MANAGER", "22446688");

  // suspend all task
  STAT_OFF;

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  server.serveStatic("/", SPIFFS, "/");
  // Web Server Root URL
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send(SPIFFS, "/views/wifimanager.html", "text/html"); });

  server.on("/", HTTP_POST, [](AsyncWebServerRequest *request)
            {
      int params = request->params();
      for(int i=0;i<params;i++){
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_SSID) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            espSPIFFS->writeSSID(SPIFFS,ssid.c_str());
          }
          // HTTP POST password value
          if (p->name() == PARAM_INPUT_PASS) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save password
            espSPIFFS->writePASS(SPIFFS, pass.c_str());
          }
          // HTTP POST ip value
          if (p->name() == PARAM_INPUT_IP) {
            ip = p->value().c_str();
            Serial.print("IP Address set to: ");
            Serial.println(ip);
            // Write file to save value
            espSPIFFS->writeIP(SPIFFS, ip.c_str()); 
          }
          // HTTP POST gateway value
          if (p->name() == PARAM_INPUT_GATEWAY) {
            gateway = p->value().c_str();
            Serial.print("Gateway set to: ");
            Serial.println(gateway);
            // Write file to save value
            espSPIFFS->writeGATEWAY(SPIFFS, gateway.c_str());
          }
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      request->send(200, "text/plain", "Done. ESP will restart, connect to your router and go to IP address: " + ip);
      // wifimanager_status = false;
      delay(3000);
      ESP.restart(); });
  server.begin();
}
void printEspReasonReset()
{
  esp_reset_reason_t reason = esp_reset_reason();
  switch (reason)
  {
  case 1:
  {
    Serial.print("Reset due to power-on event");
    break;
  }
  case 2:
  {
    Serial.print("Reset by external pin (not applicable for ESP32)");
    break;
  }
  case 3:
  {
    Serial.print("Software reset via esp_restart");
    break;
  }
  case 4:
  {
    Serial.print("Software reset due to exception/panic");
    break;
  }
  case 5:
  {
    Serial.print("Reset (software or hardware) due to interrupt watchdog");
    break;
  }
  case 6:
  {
    Serial.print("Reset due to task watchdog");
    break;
  }
  case 7:
  {
    Serial.print("Reset due to other watchdogs");
    break;
  }
  case 8:
  {
    Serial.print("Reset after exiting deep sleep mode");
    break;
  }
  case 9:
  {
    Serial.print(" Brownout reset (software or hardware)");
    break;
  }
  }
  Serial.println();
}
